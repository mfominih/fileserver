package org.testapp.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server {
    public static String STATS_FILE_NAME = "statistic.txt";
    public final static Hashtable<String, Integer> statisticMap = new Hashtable<>();
    public final static LinkedList<Session> activeFileTransferSessions = new LinkedList<>();
    private static ServerSocket serverSocket;
    private static Logger log = MainClass.getLog();

    public static void start(int port){
        try{
            if (serverSocket != null){
                return;
            }
            serverSocket = new ServerSocket(port);
            System.out.println("Server started on port: " + port + "\n");
            System.out.println("type 'stop' to stop server");
        }catch (IOException e){
            log.log(Level.WARNING, "Port " + port + " is blocked.");
            System.out.println("Port " + port + " is blocked.");
            System.exit(-1);
        }

        //write statistic
        Thread t = new Thread(new StatisticWriter());
        t.start();

        //read server commands
        Thread t1 = new Thread(new CommandReader());
        t1.start();

        log.log(Level.INFO, "server started");
        try {
            while(true) {
                Socket socket = serverSocket.accept();

                Thread thread = new Thread(new Session(socket));
                thread.start();
            }
        }catch(IOException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    /**
     * stops server
     */
    public static void stop(){
        if (!activeFileTransferSessions.isEmpty()){
            System.out.println("Server is sending file right now, are you sure?\n1 - yes\n2 - no\n");
            if (new Scanner(System.in).nextInt() == 1){

                log.log(Level.INFO, "server stopped");
                System.exit(-1);
            }
        }else {
            log.log(Level.INFO, "server stopped");
            System.exit(-1);
        }
    }

    /**
     * write statistic to file
     */
    private static class StatisticWriter implements Runnable{
        @Override
        public void run() {
            while (true){
                Properties props = new Properties();
                try {
                    File f = new File(STATS_FILE_NAME);
                    if (f.isFile() && f.canRead()) {
                        FileInputStream statsFile = new FileInputStream(f);
                        props.load(statsFile);
                    }
                } catch (IOException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    return;
                }
                synchronized (statisticMap) {
                    for (Map.Entry<String, Integer> statEntry : statisticMap.entrySet()) {
                        String prevValue = props.getProperty(statEntry.getKey());
                        long prevLongVal;
                        if (prevValue != null && !prevValue.isEmpty()){
                            prevLongVal = Long.parseLong(prevValue);
                        }else {
                            prevLongVal = 0L;
                        }
                        props.setProperty(statEntry.getKey(), String.valueOf(prevLongVal + statEntry.getValue()));
                    }
                    try {
                        FileOutputStream fos = new FileOutputStream(STATS_FILE_NAME);
                        props.store(fos, "");
                        fos.flush();
                        statisticMap.clear();
                    } catch (IOException e) {
                        log.log(Level.SEVERE, e.getMessage(), e);
                        return;
                    }
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    log.log(Level.SEVERE, e.getMessage(), e);
                    return;
                }
            }
        }
    }

    /**
     * read console command
     */
    private static class CommandReader implements Runnable{

        @Override
        public void run() {
            while (true){
                String command = new Scanner(System.in).nextLine();
                switch (command){
                    case "stop":
                        Server.stop();
                        break;
                }
            }
        }
    }
}
