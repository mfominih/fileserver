package org.testapp.server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

    public static Properties getProperties(String fileName) throws IOException {
        Properties properties = new Properties();
        InputStream propInputStream = MainClass.class.getClassLoader().getResourceAsStream(fileName);
        if (propInputStream != null){
            properties.load(propInputStream);
        }else {
            throw new FileNotFoundException(String.format("file %s not found in the classpath", fileName));
        }
        return properties;
    }

}
