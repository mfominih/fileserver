package org.testapp.server;

import java.io.*;
import java.net.Socket;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Session implements Runnable{

    public static final int STATUS_SENDING_MESSAGE = 1;
    public static final int STATUS_SENDING_FILE = 2;
    public static final int STATUS_WRONG_FILE_NUMBER = 3;
    public static final int STATUS_FILE_DOWNLOADED = 4;
    public static final int STATUS_SERVER_ERROR = 5;
    public static final int STATUS_REQUEST_FILE_LIST = 6;
    public static final int STATUS_REQUEST_FILE = 7;
    public static final int STATUS_FILE_IS_NOT_DOWNLOADED = 8;


    private final Socket socket;
    private final DataInputStream in;
    private final DataOutputStream out;
    private String filePath;
    private static Logger log = MainClass.getLog();

    public Session(Socket socket) throws IOException{
        this.socket = socket;
        this.in = new DataInputStream(this.socket.getInputStream());
        this.out = new DataOutputStream(this.socket.getOutputStream());
        this.filePath = MainClass.FILE_PATH != null ? MainClass.FILE_PATH : "";
    }


    @Override
    public void run() {
        while (!socket.isClosed())
        {
            try {
                switch (in.readInt()){
                    case STATUS_REQUEST_FILE_LIST:
                        log.log(Level.INFO, "Sending file list");
                        String fileListAsString = getFileListAsString();
                        sendStatus(STATUS_SENDING_MESSAGE);
                        sendMessage(fileListAsString);
                        break;
                    case STATUS_REQUEST_FILE:
                        log.log(Level.INFO, "Sending file");
                        int fileNumber = in.readInt();
                        List<File> fileList = getFileList();
                        if (fileNumber <= 0 || fileNumber > fileList.size()){
                            sendStatus(STATUS_WRONG_FILE_NUMBER);
                            continue;
                        }
                        sendStatus(STATUS_SENDING_FILE);
                        sendFile(fileList.get(fileNumber - 1));
                }
            } catch (IOException e) {
                log.log(Level.SEVERE, e.getMessage(), e);
                sendStatus(STATUS_SERVER_ERROR);
            }
        }
    }

    private String getFileListAsString() throws FileNotFoundException {
        List<File> fileList = getFileList();
        StringBuilder sb = new StringBuilder();
        int num = 0;
        for (File aFileList : fileList) {
            num += 1;
            sb.append(num).append(". ").append(aFileList.getName()).append("\n");
        }
        return sb.toString();
    }

    /**
     * send server status to client
     * @param status
     */
    private void sendStatus(int status){
        try {
            out.writeInt(status);
            out.flush();
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getMessage(), ex);
            closeConnection();
        }
    }

    /**
     * send message
     * @param msg
     */
    private void sendMessage(String msg){
        try {
            out.writeUTF(msg);
            out.flush();
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    /**
     * get current file list
     * @return
     */
    private List<File> getFileList() throws FileNotFoundException {
        File path = new File(this.filePath);
        if (path.isDirectory() && path.listFiles() != null) {
            LinkedList<File> files = new LinkedList<>();
            for (File file : path.listFiles()) {
                if (file.isFile()){
                    files.add(file);
                }
            }
            return files;
        }
        throw new FileNotFoundException("Error while getting file list");

    }

    /**
     * send file
     * @param file
     */
    private void sendFile(File file){

        Server.activeFileTransferSessions.add(this);
        byte[] buffer = new byte[1024];
        try {
            out.writeLong(file.length());
            out.writeUTF(file.getName());
            FileInputStream fin = new FileInputStream(file);
            int length;
            while ((length = fin.read(buffer)) != -1){
                out.write(buffer, 0, length);
                out.flush();
            }
            Server.activeFileTransferSessions.remove(this);

            if (in.readInt() != STATUS_FILE_DOWNLOADED){
                log.log(Level.SEVERE, "Error: file has been not sended");
                closeConnection();
            }
            Hashtable<String, Integer> statisticMap = Server.statisticMap;
            //update statistic
            synchronized (Server.statisticMap){
                Integer val = statisticMap.get(file.getName()) != null ? statisticMap.get(file.getName()) + 1 : 1;
                statisticMap.put(file.getName(), val);
            }
        }
        catch (IOException ex) {
            log.log(Level.SEVERE, ex.getMessage(), ex);
            closeConnection();
        }
    }

    public void closeConnection(){
        Server.activeFileTransferSessions.remove(this);
        try {
            socket.close();
            log.log(Level.INFO, "session closed");
        } catch (IOException ex) {
            log.log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
}
