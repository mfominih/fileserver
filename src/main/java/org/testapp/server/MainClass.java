package org.testapp.server;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class MainClass {
    public static final int DEFAULT_PORT = 5555;
    public static final String FILE_PATH_PROPERTY_NAME = "filePath";
    public static final String PROP_FILE_NAME = "config.properties";

    private static Logger log = Logger.getLogger(MainClass.class.getName());
    public static String FILE_PATH;

    public static void main(String[] args) {

        configureLogging();

        Properties properties = null;
        try {
            properties = PropertiesReader.getProperties(PROP_FILE_NAME);
            FILE_PATH = properties.getProperty(FILE_PATH_PROPERTY_NAME);
        } catch (IOException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
        }
        if (properties == null) {
            log.log(Level.SEVERE, "Cannot read properties");
            System.exit(-1);
        }
        int port;
        try{
            port = Integer.parseInt(properties.getProperty("port"));
            if (port < 1 || port > 65535){
                log.log(Level.SEVERE, "Wrong port value in properties file");
                port = DEFAULT_PORT;
            }
        }
        catch (NumberFormatException e) {
            log.log(Level.SEVERE, "Wrong port value in properties file: ", e);
            port = DEFAULT_PORT;
        }
        Server.start(port);
    }

    public static void configureLogging(){
        FileHandler fh;

        try {
            fh = new FileHandler("server.log", 1000000, 10);
            log.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            log.setUseParentHandlers(false);
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    public static Logger getLog() {
        return log;
    }
}
